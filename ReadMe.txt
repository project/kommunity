Make sure the following directories and files are writeable by the server:

/layout.css
/color/color.inc
/images/text/cache/
/images/text.js



DEVELOPMENT VERSION

Please file bugs in the issue queue.

*Still to be done: 
  - Preview on the theme settings page
  - Remove forum "forward" and "backward" links (possibly with on/off-toggle on theme settings page)
  - Theme user profile page
  - Add icons to various links (possibly with on/off-toggle on theme settings page)
  - Theme node author's comments different than normal comments
  - Add search region on the right side of the breadcrumb area (possibly with on/off-toggle on theme settings page)
  - Add various regions
  - Add header region for images, possibly with ability to assign specific image to specific pages (similar to block visibility settings) on theme settings page
  - Fixing minor problems on the theme settings page
  - Extensive testing