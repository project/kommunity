<?php

/**
 * Include the global theme-settings.inc.
 */
 include(realpath(dirname(__FILE__) .'/template.php'));
 
/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
    'kommunity_page_width'         => '75%',
	'kommunity_sidebar_width'      => '210px',
	'kommunity_border'             => 0,
	'kommunity_text_header'        => 0,
	'kommunity_text_header_font'   => '',
	'kommunity_text_content'       => 0,
	'kommunity_text_content_font'  => '',
	'kommunity_text_comment'       => 0,
	'kommunity_text_comment_font'  => '',
	'kommunity_comment_author'     => 0,

  );
  
  // Reset to default value if the user specified setting is invalid
  $saved_page_width = $saved_settings['kommunity_page_width'];
  $saved_settings['kommunity_page_width'] = kommunity_validate_page_width($saved_page_width);
  $saved_sidebar_width = $saved_settings['kommunity_sidebar_width'];
  $saved_settings['kommunity_sidebar_width'] = kommunity_validate_sidebar_width($saved_sidebar_width);

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);
  
  $fonts = file_scan_directory('sites/all/themes/kommunity/images/text/fonts','.otf|.ttf|.OTF|.TTF');
  $font_options = array();
  foreach($fonts as $font) {
  	$font_options[$font->basename] = $font->name;
  }

  // Create the form widgets using Forms API
  
  $form['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Options to change layout and general appearance'),
    '#attributes' => array('id' => 'theme-settings-layout')
  );
  
  $form['layout']['kommunity_page_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Page width'),
    '#default_value' => $settings['kommunity_page_width'],
    '#size' => 12,
    '#maxlength' => 8,
    '#description' => t('Specify the page width in percent ratio (50-95%) for liquid layout, or in px (700-1300px) for fixed width layout. If an invalid value is specified, the default value (75%) is used instead. You can leave this field blank to use the default value. You need to add either % or px after the number.'),
  );
  
  $form['layout']['kommunity_sidebar_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar width'),
    '#default_value' => $settings['kommunity_sidebar_width'],
    '#size' => 12,
    '#maxlength' => 8,
    '#description' => t('Specify the sidebar width in px (150-500px). If an invalid value is specified, the default value (210px) is used instead. You can leave this field blank to use the default value. You need to add px after the number.'),
  );
  
  $form['border'] = array(
    '#type' => 'fieldset',
    '#title' => t('Border'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Option to change appearance of border around basic elements.'),
    '#attributes' => array('id' => 'theme-settings-border')
  );
  
  $form['border']['kommunity_border'] = array(
    '#type' => 'select',
    '#title' => t('Border style'),
    '#default_value' => $settings['kommunity_border'],
    '#options' => array(
        0 => 'thin (1px)',
        1 => 'thick (5px)',
    ),
	'#description' => t('Specify the thickness of the border of page and sidebar. Either thin (1px), which is the default setting, or thicker (5px).'),
  );
  
  $form['text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Text'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Option to change appearance of website and content titles.'),
    '#attributes' => array('id' => 'theme-settings-text')
  );
	
	if (count($font_options)==0) {
		$form['text']['#description'] = t('Option to change appearance of website and content titles.<br /><br />Before enabling this option, some Truetype fonts (.ttf) or Opentype fonts (.otf) must be uploaded to the \'themes/kommunity/images/text/fonts\' folder.');
	} else {
	
	  $form['text']['header'] = array(
          '#type' => 'fieldset',
          '#title' => t('Website title'),
          '#collapsible' => true,
          '#collapsed' => true,
          '#description' => t('Appearance of the website title.'),
          '#attributes' => array('id' => 'theme-settings-text-header')
      );
	  
		$form['text']['header']['kommunity_text_header'] = array(
			'#type' => 'checkbox',
			'#title' => t('Use font for website title'),
			'#default_value' => $settings['kommunity_text_header'],
			'#return_value' => 1,
			'#disabled' => (count($font_options)==0)?TRUE:FALSE,
			'#required' => FALSE
		);
		
		$form['text']['header']['kommunity_text_header_font'] = array(
			'#type' => 'select',
			'#title' => t('Truetype/Opentype font'),
			'#default_value' => $settings['kommunity_text_header_font'],
			'#options' => $font_options,
			'#description' => t('Choose a font from the drop-down list'),
			'#disabled' => ($settings['kommunity_text_header']==0)?TRUE:FALSE,
			'#multiple' => FALSE,
			'#required' => FALSE
		);
		
	    $form['text']['content'] = array(
          '#type' => 'fieldset',
          '#title' => t('Content title'),
          '#collapsible' => true,
          '#collapsed' => true,
          '#description' => t('Appearance of the content titles.'),
          '#attributes' => array('id' => 'theme-settings-text-content')
        );
		
		$form['text']['content']['kommunity_text_content'] = array(
			'#type' => 'checkbox',
			'#title' => t('Use font for content titles'),
			'#default_value' => $settings['kommunity_text_content'],
			'#return_value' => 1,
			'#disabled' => (count($font_options)==0)?TRUE:FALSE,
			'#required' => FALSE
		);
		
		$form['text']['content']['kommunity_text_content_font'] = array(
			'#type' => 'select',
			'#title' => t('Truetype/Opentype font'),
			'#default_value' => $settings['kommunity_text_content_font'],
			'#options' => $font_options,
			'#description' => t('Choose a font from the drop-down list'),
			'#disabled' => ($settings['kommunity_text_content']==0)?TRUE:FALSE,
			'#multiple' => FALSE,
			'#required' => FALSE
		);
		
		$form['text']['comment'] = array(
          '#type' => 'fieldset',
          '#title' => t('Comment title'),
          '#collapsible' => true,
          '#collapsed' => true,
          '#description' => t('Appearance of the comment titles.'),
          '#attributes' => array('id' => 'theme-settings-text-comment')
        );
		
		$form['text']['comment']['kommunity_text_comment'] = array(
			'#type' => 'checkbox',
			'#title' => t('Use font for comment titles'),
			'#default_value' => $settings['kommunity_text_comment'],
			'#return_value' => 1,
			'#disabled' => (count($font_options)==0)?TRUE:FALSE,
			'#required' => FALSE
		);
		
		$form['text']['comment']['kommunity_text_comment_font'] = array(
			'#type' => 'select',
			'#title' => t('Truetype/Opentype font'),
			'#default_value' => $settings['kommunity_text_comment_font'],
			'#options' => $font_options,
			'#description' => t('Choose a font from the drop-down list'),
			'#disabled' => ($settings['kommunity_text_comment']==0)?TRUE:FALSE,
			'#multiple' => FALSE,
			'#required' => FALSE
		);
		
	    $form['comment'] = array(
          '#type' => 'fieldset',
          '#title' => t('Comment author'),
          '#collapsible' => true,
          '#collapsed' => true,
          '#description' => t('Appearance of the comments of the author of a node.'),
          '#attributes' => array('id' => 'theme-settings-comment')
        );
	  
		$form['comment']['kommunity_comment_author'] = array(
			'#type' => 'checkbox',
			'#title' => t('Special markup for node author\'s comments'),
			'#default_value' => $settings['kommunity_comment_author'],
			'#return_value' => 1,
			'#required' => FALSE
		);
		
	}

  // Return the additional form widgets
  return $form;
}

?>